package com.vtrio.api;

import java.util.List;

import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vtrio.model.Product;
import com.vtrio.util.CSVToJavaUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "RuleController", description = "REST APIs related to Drolls POC!!!!")
@RestController
public class InventoryController {
	
	@Autowired
	private KieSession session;
	
	@ApiOperation(value = "Get discounts for a given product ", response = Iterable.class)	
	@PostMapping("/api/vtrio/discount")
	public Product getDiscount(@RequestBody  Product product) {
		session.insert(product);
		session.fireAllRules();
		return product;
		
	}
	
	@ApiOperation(value = "Get discounts for list of products ", response = Iterable.class)	
	@PostMapping("/api/vtrio/discounts")
	public List<Product> getDiscounts(@RequestBody  List<Product> products) {
		
		for(Product product: products) {
			session.insert(product);
		}
		session.fireAllRules();
		return products;
		
	}
	
	
	@ApiOperation(value = "Get discounts for product defined in data/products.csv ", response = Iterable.class)	
	@GetMapping("/api/vtrio/discounts")
	public List<Product> getDiscounts() throws Exception {
		List<Product> products = CSVToJavaUtil.getProdutsFromCsv();
		for(Product product: products) {
			session.insert(product);
		}
		session.fireAllRules();
		return products;
		
	}

}
