package com.vtrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocDroolsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocDroolsApplication.class, args);
	}

}
