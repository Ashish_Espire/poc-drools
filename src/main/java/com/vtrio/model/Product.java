package com.vtrio.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long productId;
	private String productCode;
	private String productName;
	private String productType;
	private Long price;
	private Double efectivePrice;
	private Long discount;
	private Boolean sale;
	private String caseRule;
	private String items;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	// @CsvDate(value = "yyyy-MM-dd")
	// @CsvBindByPosition(position = 8)
	private Date orderDate = new Date();

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getDiscount() {
		return discount;
	}

	public void setDiscount(Long discount) {
		this.discount = discount;
	}

	public Boolean getSale() {
		return sale;
	}

	public void setSale(Boolean sale) {
		this.sale = sale;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getCaseRule() {
		return caseRule;
	}

	public void setCaseRule(String caseRule) {
		this.caseRule = caseRule;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public Double getEfectivePrice() {
		if (null == efectivePrice) {
			efectivePrice = new Double(price);
		}
		return efectivePrice;
	}

	public void setEfectivePrice(Double efectivePrice) {
		this.efectivePrice = efectivePrice;
	}

	public void updatePrice(Long discount) {
		this.discount = discount;
		efectivePrice = efectivePrice - ((efectivePrice * discount) / 100);
		System.out.println("update price called " + efectivePrice);
		updateCase();

	}

	public void updateCase() {
		int index = 1;
		String caseR = "U";
		String[] caseRules = caseRule.split(",");
		String[] itemsI = items.split(",");

		if (null != caseRules && caseRules.length == 2) {
			index = Integer.parseInt(caseRules[0]);
			caseR = caseRules[1].toUpperCase();
		}
		
		if(index < 0) {
			return;
		}

		if (index == 0 && caseRules.length > 0) {
			for (int i = 0; i < caseRules.length; i++) {
				switch (caseR) {
				case "L":
					itemsI[i] = itemsI[i].toLowerCase();
					break;
				case "U":
					itemsI[i] = itemsI[i].toUpperCase();
					break;

				}

				this.items = String.join(",", itemsI);
				return;
			}
		} else {
			index = index-1;
			if (null != itemsI && itemsI.length >= index) {
				switch (caseR) {
				case "L":
					itemsI[index] = itemsI[index].toLowerCase();
					break;
				case "U":
					itemsI[index] = itemsI[index].toUpperCase();
					break;

				}

				this.items = String.join(",", itemsI);
				switch (caseR) {
				case "L":
					itemsI[index] = itemsI[index].toLowerCase();
					break;
				case "U":
					itemsI[index] = itemsI[index].toUpperCase();
					break;

				}

				this.items = String.join(",", itemsI);
			}

		}
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productCode=" + productCode + ", productName=" + productName
				+ ", productType=" + productType + ", price=" + price + ", discount=" + discount + ", sale=" + sale
				+ ", caseRule=" + caseRule + ", items=" + items + ", efectivePrice=" + efectivePrice + ", orderDate="
				+ orderDate + "]";
	}

}
