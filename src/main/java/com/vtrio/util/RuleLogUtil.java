package com.vtrio.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vtrio.DebugAgendaEventListener;
import com.vtrio.model.Product;

public class RuleLogUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DebugAgendaEventListener.class);

	public static void logProduct(String ruleName, Product product) {
		StringBuilder logMessage = new StringBuilder();
		logMessage.append("Before Rule : ");
		logMessage.append(ruleName);
		logMessage.append(", \t");
		logMessage.append("The effective price of product id  ");
		logMessage.append(product.getProductId());
		logMessage.append(" is  : ");
		logMessage.append(product.getEfectivePrice());
		logMessage.append(" , case rule  : ");
		logMessage.append(product.getCaseRule());
		logMessage.append(" , items  : ");
		logMessage.append(product.getItems());
		LOGGER.info(logMessage.toString());
	}
	
	public static void logRule(String ruleName, Product product) {
		StringBuilder logMessage = new StringBuilder();
		logMessage.append("After Rule ");
		logMessage.append(ruleName);
		logMessage.append(", \t");
		logMessage.append("The effective price of product id  ");
		logMessage.append(product.getProductId());
		logMessage.append(" is updated to : ");
		logMessage.append(product.getEfectivePrice());
		logMessage.append("  .  ");
		logMessage.append("Product information : \t ");
		logMessage.append(" Product Id: ");
		logMessage.append(product.getProductId());
		logMessage.append(", Product name : ");
		logMessage.append(product.getProductName());
		logMessage.append(", Product Type: ");
		logMessage.append(product.getProductType());
		logMessage.append(": Discount is updated to ");
		logMessage.append(product.getDiscount());
		logMessage.append(" % .");
		logMessage.append(": Effective Price is updated to ");
		logMessage.append(product.getEfectivePrice());
		logMessage.append(" , case rule  : ");
		logMessage.append(product.getCaseRule());
		logMessage.append(" , items  : ");
		logMessage.append(product.getItems());
		
		

		LOGGER.info(logMessage.toString());
	}
	
	

}
