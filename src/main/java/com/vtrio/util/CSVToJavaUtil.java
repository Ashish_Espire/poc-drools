package com.vtrio.util;

import java.io.FileReader;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.vtrio.model.Product;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class CSVToJavaUtil {
	
	private static final String csvFilename = "data/products.csv";
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<Product> getProdutsFromCsv() throws Exception{
		
		CsvToBean csv = new CsvToBean();

		ClassPathResource resource = new ClassPathResource(csvFilename);
		FileReader fileReader = new FileReader(resource.getFile());

		CSVReader csvReader = new CSVReader(fileReader, ',');

		List<Product> products = csv.parse(setColumMapping(), csvReader);
		
		return products;
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ColumnPositionMappingStrategy setColumMapping() {
		ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
		strategy.setType(Product.class);
		String[] columns = new String[] { "productId", "productCode", "productName", "productType", "price", "discount", "sale","caseRule","items" };
		strategy.setColumnMapping(columns);
		return strategy;
	}

}
