package com.vtrio;

import java.io.FileReader;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.vtrio.model.Product;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class CSVToJavaBeanExample {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws Exception {

		CsvToBean csv = new CsvToBean();

		String csvFilename = "data/products.csv";
		
		 ClassPathResource resource = new ClassPathResource(csvFilename);
		 FileReader fileReader = new FileReader(resource.getFile());

		CSVReader csvReader = new CSVReader(fileReader, ',');

		List list = csv.parse(setColumMapping(), csvReader);

		for (Object object : list) {
			Product product = (Product) object;
			System.out.println(product);
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ColumnPositionMappingStrategy setColumMapping() {
		ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
		strategy.setType(Product.class);
		String[] columns = new String[] { "productId", "productCode", "productName", "productType", "price", "discount", "sale" };
		strategy.setColumnMapping(columns);
		return strategy;
	}
}
