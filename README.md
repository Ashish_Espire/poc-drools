# Getting Started

### Reference Documentation
It is a sample project on Drools with Spring Boot. Below are some key detils of this poc

1) Three rest endpoint (POST) has been provided for testing of the rules

   A ) To get discount of a particular product

     URL :    http://localhost:9090/api/vtrio/discount
	  Method : HTTP POST
	  Payload : Content-Type: application/json
	           {
					"productName": "Ring",
					"productType": "gold",
					"price": 12000,
					"sale" : "true"
				}
				
	B) Get discount of multiple products
	
		URL :    http://localhost:9090/api/vtrio/discounts
		Method : HTTP POST
		Payload : Content-Type: application/json
	           [
				{

					"productName": "Ring",
					"productType": "gold",
					"price": 15001
				},
				{

					"productName": "Ring",
					"productType": "silver",
					"price": 15001,
					"orderDate": "25-12-2020"
				}

			]
			
			
	c) Get discount of the products defined in csv 			(resources/data/products.csv)
	
		URL :    http://localhost:9090/api/vtrio/discounts
		Method : HTTP GET
		
			
	 Note : 
	 1) Rest calss is com.vtrio.api.RuleController.java		
	 2) Swagger ui :http://localhost:9090/swagger-ui.html

	

2) Two rules files has been used a) rule.drl and rules.xls

   A) rule.drl defines discount on productType (platinum and silver) and price . It also define discount of orderDate is 25 December (xms)
       and if sale="true"
	   
   B) rules.xls define discount on productType (gold and diamond)	and price range   
   
      Example : if productType is diamond and price is >=15000 and price < 100000l discout is 15
	            if productType is gold and price is >=10000 and price < 50000l discout is 14
				
	Note : Rule File path is src/main/resources/rules folder , the Sconfiguration class for Drools for Spring boot is 
           com.vtrio.config.DroolConfig.java
		   
		   
Note : 
1) Port (9090) for this Spring boot drolls web app is defined in src\main\resources\application.properties	   
2) Log file for rule will be located at logs/rule-logger.log folder


 	

